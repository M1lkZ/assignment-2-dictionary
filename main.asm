%define MAGIC_MAX 255
%define MAGIC_SHIFT 8
%define MAGIC_ERR 1
extern exit
extern read_word
extern string_length
extern print_string
extern print_error
extern print_newline
extern find_word

section .bss
buff: resb 255

section .rodata
error_buff_size_msg: db "String is too long, max is 255 symbols", 10, 0
error_key_msg: db "No such key found", 10, 0

%include "words.inc"

section .text
global _start
_start:
	mov rdi, buff
	mov rsi, MAGIC_MAX
	call read_word
	test rax, rax
	je .buff_out
	mov rsi, next_element
	mov rdi, buff
	call find_word
	cmp rax, 0
	je .no_key
	mov rdi, rax
	add rdi, MAGIC_SHIFT
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax
	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	call exit
	.no_key:
		mov rdi, error_key_msg
		call print_error
		mov rdi, MAGIC_ERR
		call exit
	.buff_out:
		mov rdi, error_buff_size_msg
		call print_error
		mov rdi, MAGIC_ERR
    	call exit
