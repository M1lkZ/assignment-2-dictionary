%define MAGIC_SHIFT 8
global find_word
extern string_equals
section .text

find_word:
	push r12
	push r13
	xor r8, r8
	.loop:
		mov r12, rsi
		mov r13, rdi
		add rsi, MAGIC_SHIFT
		call string_equals
		mov rsi, r12
    	mov rdi, r13 
		cmp rax, 1
		je .found
		mov qword r8, [rsi]
    	cmp r8, 0
		jz .not_found
		mov rsi, r8
		jmp .loop
	.found:
		mov rax, rsi
		pop r13
		pop r12
		ret  
	.not_found:
		xor rax, rax
		ret