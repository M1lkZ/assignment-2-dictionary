ASM = nasm
FLAGS = -felf64 -i
LINKER = ld
LINKER_FLAGS = -o
OUT_NAME = main

.phony: all clean
all: main.o lib.o dict.o
	$(LINKER) $(LINKER_FLAGS) $(OUT_NAME) $+


%.o: %.asm
	$(ASM) $(FLAGS) $@ $<

clean:
	rm -rf *.o
